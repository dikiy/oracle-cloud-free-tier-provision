variable "VCN-CIDR" { default = "10.0.0.0/16" }

resource "oci_core_vcn" "vcn1" {
  cidr_block     = "${var.VCN-CIDR}"
  dns_label      = "vcn1"
  compartment_id = "${local.compartment_ocid}"
  display_name   = "vcn1"
}


resource "oci_core_subnet" "vcn1_subnet1" {
  cidr_block     = "10.0.10.0/24"
  compartment_id = "${local.compartment_ocid}"
  vcn_id         = "${oci_core_vcn.vcn1.id}"
  display_name   = "Main public subnet in global AD"
  dns_label      = "main"
}

resource "oci_core_internet_gateway" "inet_gw" {
  compartment_id = "${local.compartment_ocid}"
  vcn_id         = "${oci_core_vcn.vcn1.id}"
  display_name   = "Internet gateway for vcn1"
}

resource "oci_core_default_route_table" "default_route_table" {
  manage_default_resource_id = "${oci_core_vcn.vcn1.default_route_table_id}"
  display_name   = "Default Route Table for vcn1 network"

  route_rules {
    destination       = "0.0.0.0/0"
    destination_type  = "CIDR_BLOCK"
    network_entity_id = "${oci_core_internet_gateway.inet_gw.id}"
  }
}


resource "oci_core_default_dhcp_options" "default_dhcp_options" {
  manage_default_resource_id = "${oci_core_vcn.vcn1.default_dhcp_options_id}"
  display_name   = "Default DHCP options for vcn1"

  // required
  options {
    type        = "DomainNameServer"
    server_type = "VcnLocalPlusInternet"
  }

  // optional
  options {
    type                = "SearchDomain"
    search_domain_names = ["vcn1.oraclevcn.com"]
  }
}

resource "oci_core_default_security_list" "default_security_list" {
  manage_default_resource_id = "${oci_core_vcn.vcn1.default_security_list_id}"
  display_name               = "defaultSecurityList"

  // allow all outbound traffic
  egress_security_rules {
    destination = "0.0.0.0/0"
    protocol    = "all"
  }

  // allow inbound ssh traffic
  ingress_security_rules {
    protocol  = "6"         // tcp
    source    = "0.0.0.0/0"
    stateless = false

    tcp_options {
      min = 22
      max = 22
    }
  }

  // allow inbound http traffic
  ingress_security_rules {
    protocol  = "6"         // tcp
    source    = "0.0.0.0/0"
    stateless = false

    tcp_options {
      min = 80
      max = 80
    }
  }

  // allow inbound https traffic
  ingress_security_rules {
    protocol  = "6"         // tcp
    source    = "0.0.0.0/0"
    stateless = false

    tcp_options {
      min = 443
      max = 443
    }
  }

  // allow inbound internal icmp unreach of all types
  ingress_security_rules {
    protocol    = 1
    source      = "${oci_core_vcn.vcn1.cidr_block}"
    source_type = "CIDR_BLOCK"
    stateless   = false

    icmp_options {
      code = -1
      type = 3
    }
  }

  // allow inbound icmp traffic of a specific type
  ingress_security_rules {
    protocol  = 1
    source    = "0.0.0.0/0"

    icmp_options {
      type = 3
      code = 4
    }
  }
}
