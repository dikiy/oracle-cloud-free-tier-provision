variable "region" {
  default = "eu-frankfurt-1"
}
variable "tenancy_ocid" {}
variable "user_ocid" {}
variable "fingerprint" {}

# https://docs.cloud.oracle.com/iaas/Content/API/Concepts/apisigningkey.htm
variable "private_key_path" {
  default = "~/.oci/oci_api_key.pem"
}

variable "compartment_ocid" {
  default = ""
}

locals {
 compartment_ocid = "${var.compartment_ocid != "" ? var.compartment_ocid : "${var.tenancy_ocid}"}"
}

provider "oci" {
  version          = ">= 3.0.0"
  region           = "${var.region}"
  tenancy_ocid     = "${var.tenancy_ocid}"
  user_ocid        = "${var.user_ocid}"
  fingerprint      = "${var.fingerprint}"
  private_key_path = pathexpand("${var.private_key_path}")
}

