/etc/systemd/system/nginx.service.d/nginx-override.conf:
  file.managed:
    - makedirs: True
    - contents: |
        [Service]
        ExecStartPost=/bin/sleep 0.1
'systemctl daemon-reload':
  cmd.run:
    - onchanges:
      - file: /etc/systemd/system/nginx.service.d/nginx-override.conf

include:
  - nginx
  - php.fpm