nginx:
  lookup:
    server_available: /etc/nginx/sites-available
    server_enabled: /etc/nginx/sites-enabled
    server_use_symlink: true
  server:
    config:
      worker_processes: 1
  snippets:
    letsencrypt.conf:
      - location ^~ /.well-known/acme-challenge/:
          - proxy_pass: http://localhost:9999
  servers:
    managed:
      baznikin.ru:
        enabled: true
        config:
          - server:
              - server_name: baznikin.ru
              - listen:
                  - '80 default_server'
                  - '443 ssl'
              - index: 'index.html index.htm'
              - location ~ .htm:
                  - try_files: '$uri $uri/ =404'
              - include: 'snippets/letsencrypt.conf'

