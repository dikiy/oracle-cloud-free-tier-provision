variable "ssh_keys" {}

variable "num_instances" {
  default = "1"
}

variable "instance_shape" {
  default = "VM.Standard.E2.1.Micro"
}

variable "instance_image_ocid" {
  type = "map"

  default = {
    # See https://docs.cloud.oracle.com/iaas/images/oraclelinux-7x/
    # Oracle-provided image "Oracle-Linux-7.7-2019.09.25-0"
    eu-frankfurt-1 = "ocid1.image.oc1.eu-frankfurt-1.aaaaaaaajqghpxnszpnghz3um66jywaw5q3pudfw5qwwkyu24ef7lcsyjhsq"
  }
}

resource "oci_core_instance" "test_instance" {
  count               = "${var.num_instances}"
  availability_domain = "${data.oci_identity_availability_domain.ad.name}"
  compartment_id      = "${local.compartment_ocid}"
  display_name        = "TestInstance${count.index}"
  shape               = "${var.instance_shape}"
  fault_domain        = "FAULT-DOMAIN-${count.index + 1}"

  create_vnic_details {
    subnet_id        = "${oci_core_subnet.vcn1_subnet1.id}"
    display_name     = "Primaryvnic"
    assign_public_ip = true
    hostname_label   = "instance${count.index}"
  }

  source_details {
    source_type = "image"
    source_id   = "${var.instance_image_ocid[var.region]}"

    # Apply this to set the size of the boot volume that's created for this instance.
    # Otherwise, the default boot volume size of the image is used.
    # This should only be specified when source_type is set to "image".
    #boot_volume_size_in_gbs = "60"
  }

  # Apply the following flag only if you wish to preserve the attached boot volume upon destroying this instance
  # Setting this and destroying the instance will result in a boot volume that should be managed outside of this config.
  # When changing this value, make sure to run 'terraform apply' so that it takes effect before the resource is destroyed.
  #preserve_boot_volume = true

  metadata = {
    ssh_authorized_keys = "${var.ssh_keys}"
    user_data           = "${base64encode(file("./userdata/bootstrap"))}"
  }
  timeouts {
    create = "60m"
  }
}

#resource "oci_core_image" "custom_image" {
#  count          = "${oci_core_instance.test_instance.count}"
#  compartment_id = "${local.compartment_ocid}"
#  instance_id    = "${oci_core_instance.test_instance[count.index].id}"

#  launch_mode = "NATIVE"

#  timeouts {
#    create = "30m"
#  }
#}

data "oci_identity_availability_domain" "ad" {
  compartment_id = "${var.tenancy_ocid}"
  ad_number      = 1
}

 //////////////////////////////////////////
 // Salt Config
 //////////////////////////////////////////
resource "null_resource" "salt" {
  depends_on = ["oci_core_instance.test_instance"]
  count      = length(oci_core_instance.test_instance)

  connection {
    user                = "opc"
    private_key         = "${file(pathexpand("~/.ssh/id_rsa"))}"
    host                = "${oci_core_instance.test_instance[count.index].public_ip}"
    timeout             = "4m"
  }

  # workaround https://github.com/hashicorp/terraform/issues/20323
  provisioner "remote-exec" {
    inline = [
      "sudo rm -rf /srv/salt /srv/pillar",
    ]
  }

  provisioner "salt-masterless" {
    local_pillar_roots   = "salt_tree/pillar/"
    local_state_tree   = "salt_tree/salt/"
    minion_config_file = "salt_tree/config"
    bootstrap_args     = "-X -d"
  }

  provisioner "remote-exec" {
    inline = [
#      "sudo mkdir /srv/testest",
    ]
  }
}

output "instance_ip" {
  value = "${oci_core_instance.test_instance.*.public_ip}"
}